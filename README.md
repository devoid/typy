# Project status

![Build](https://gitlab.com/devoid/typy/badges/master/build.svg)
![Coverage](https://gitlab.com/devoid/typy/badges/master/coverage.svg)

# What is it?

**Typy** is a python app for creating and managing a static site from structurized data.

In most cases you don't need big servers (python/php/ruby/mysql/postgresql etc.) to host your site - you can build it yourself, and send generated html to simple hosting server.

Ofcourse if you need fast changes and really depend on databases, then **Typy** isn't best choice for you.

But if you want fast, reliable site with many available themes, and you are not afraid to keep your structure in simple text files, then **Typy** should work for you.

## Similar projects

**Typy** was inspred by Go project called Hugo.

# Install typy

For now, just clone it from gitlab.com:

```bash
git clone https://gitlab.com/devoid/typy.git
```

# Run tests

## Normal tests (python2.7+)

In project directory run:

```bash
python -m unittest discover
```

## Coverage

If you want to use coverage, run:

```bash
coverage run -m unittest discover
```

Then you can check code coverage by using command:
```bash
coverage report
```
