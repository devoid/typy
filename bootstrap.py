import os
import shutil

from config import (BOOTSTRAP_CONFIG, CONFIG_FILE, DEFAULT_THEME_DIR,
                    DIR_STRUCTURE, FILE_STRUCTURE, SAMPLE_THEME_DIR,
                    THEMES_DIR)

from data import MetaData


class Bootstrap(object):
    """Bootstrap class

    Creates bootstrap directory/files structure.


    """

    def __init__(self, start_dir="./", config=BOOTSTRAP_CONFIG, dirs=DIR_STRUCTURE, files=FILE_STRUCTURE):
        super(Bootstrap, self).__init__()
        self.start_dir = start_dir
        self.config = config
        self.dirs = dirs
        self.files = files

    def start(self):
        for directory in self.dirs:
            full_dir = os.path.join(self.start_dir, directory)
            if not os.path.exists(full_dir):
                os.makedirs(full_dir)

        for f in self.files:
            full_path = os.path.join(self.start_dir, f)
            fh = open(full_path, 'w+')
            fh.close()

        fh = open(os.path.join(self.start_dir, CONFIG_FILE), 'w+')
        meta_data = MetaData(self.start_dir)
        fh.write(meta_data.build_metadata(self.config))
        fh.close()

        dest_dir = os.path.join(self.start_dir, THEMES_DIR, SAMPLE_THEME_DIR)
        shutil.copytree(DEFAULT_THEME_DIR, dest_dir)
