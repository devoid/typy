# -*- coding: utf-8 -*-
import os
import shutil

from config import PUBLIC_DIR, STATIC_DIR, THEMES_DIR
from constructor import Constructor
from data import Data, MetaData
from project import Project


class Builder(Project):
    """Builder class

    Builds static version of site.
    """

    def __init__(self, start_dir="./"):
        super(Builder, self).__init__(start_dir)
        self.start_dir = start_dir
        meta_data = MetaData(self.start_dir)
        self.site_metadata = meta_data.gather_config_metadata()
        self.site_contenttypes = meta_data.gather_contenttypes()

    def build(self):
        self._cleanup()

        # gather objects
        self.site_data = Data(self.start_dir)
        site_objects = self.site_data.get_content_data()

        # copy static files
        dest_static_dir = os.path.join(self.start_dir, PUBLIC_DIR, STATIC_DIR)
        source_static_dir = os.path.join(self.start_dir, THEMES_DIR, self.site_metadata[
                                         'site']['theme'], STATIC_DIR)
        shutil.copytree(source_static_dir, dest_static_dir)
        # * create directories and index.htmls based on objects in destination_path
        constructor = Constructor(self.start_dir, metadata=self.site_metadata)
        constructor.construct(site_objects)

    def _cleanup(self):
        shutil.rmtree(os.path.join(self.start_dir, PUBLIC_DIR))
        os.makedirs(os.path.join(self.start_dir, PUBLIC_DIR))
