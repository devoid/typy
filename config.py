# -*- coding: utf-8 -*-
import datetime

FRONT_MATTER = {
    'categories': ["default", ],
    'description': "",
    'datetime': datetime.datetime.now(),
}

SAMPLE_THEME_DIR = "typy-ernest"

BOOTSTRAP_CONFIG = {
    'site': {
        'baseurl': 'http://example.com',
        'languagecode': 'en_EN',
        'disqus_shortname': 'exampl-1',
        'theme': SAMPLE_THEME_DIR,
    },
    'author': {
        'name': 'your_name',
    },
    'params': {
        'tagline': 'Example site. Welcome!',
    },
}

CONTENT_DIR = "content"
LAYOUTS_DIR = "layouts"
PUBLIC_DIR = "public"
STATIC_DIR = "static"
THEMES_DIR = "themes"
CONFIG_FILE = "config.yaml"
DEFAULT_THEME_DIR = "defaults/theme/typy-ernest"

DIR_STRUCTURE = [

    CONTENT_DIR,
    LAYOUTS_DIR,
    PUBLIC_DIR,
    THEMES_DIR,
]

FILE_STRUCTURE = [
    CONFIG_FILE,
]
