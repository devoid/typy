# -*- coding: utf-8 -*-

import os
from jinja2 import Environment, FileSystemLoader, Template

from config import LAYOUTS_DIR, SAMPLE_THEME_DIR, THEMES_DIR
from project import Project


class Constructor(Project):

    def __init__(self, *args, **kwargs):
        self.metadata = kwargs.pop('metadata', {})
        super(Constructor, self).__init__(*args, **kwargs)
        self.env = Environment(loader=FileSystemLoader(
            'test_site/themes/typy-ernest/layouts'))

    def construct(self, object_list):
        print(self.metadata)
        print(self.render('index.html', self.metadata))

    def render(self, template_name, context):
        template = self.env.get_template(template_name)
        return template.render(**context)

    def _render_template(self, template_name, context):
        with open(template_name) as template_html:
            return self._render_from_text(template_html.read(), context)

    def _render_from_text(self, template_html, context):
        template = Template(template_html)
        return template.render(context)
