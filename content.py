# -*- coding: utf-8 -*-
import os
import yaml

from config import CONTENT_DIR, FRONT_MATTER
from project import Project


class New(Project):
    """New class

    Creates new content in existing structure.

    """

    def __init__(self, start_dir="./", front_matter=FRONT_MATTER):
        super(New, self).__init__(start_dir)
        self.start_dir = start_dir
        self.front_matter = front_matter

    def create(self, path):
        full_path = os.path.join(self.start_dir, CONTENT_DIR, path)
        d = os.path.dirname(full_path)
        if not os.path.exists(d):
            os.makedirs(d)
        fh = open(full_path, 'w+')
        fh.write("+++\n")
        fh.write(yaml.dump(self.front_matter, default_flow_style=False))
        fh.write("+++\n")
        fh.close()
