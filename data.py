# -*- coding: utf-8 -*-


import os
import yaml

from config import BOOTSTRAP_CONFIG, CONFIG_FILE, CONTENT_DIR
from objects import Content, FrontMatter
from project import Project


class MetaData(Project):
    """Metadata class

    Responsible for site metadata.
    """

    def __init__(self, start_dir="./"):
        super(MetaData, self).__init__(start_dir)
        self.start_dir = start_dir

    def gather_config_metadata(self):
        config_path = os.path.join(self.start_dir, CONFIG_FILE)
        fh = open(config_path, "r")
        config = yaml.safe_load(fh.read())
        fh.close()
        return config

    def build_metadata(self, config):
        return yaml.dump(config if config else BOOTSTRAP_CONFIG, default_flow_style=False)

    def gather_contenttypes(self):
        content_path = os.path.join(self.start_dir, CONTENT_DIR)
        content_types = []
        for element in os.listdir(content_path):
            if os.path.isdir(os.path.join(content_path, element)):
                content_types.append(element)

        return content_types


class Data(Project):

    def __init__(self, start_dir="./"):
        super(Data, self).__init__(start_dir)
        self.start_dir = start_dir

    def get_content_data(self):
        result = []
        for f in self._get_file_list():
            obj = Content(self._get_front_matter(
                f), content_type=self._get_content_type(f), file_path=f)
            obj.body = self._get_body(f)
            result.append(obj)
        return result

    def _get_front_matter(self, f_name):
        full_path = os.path.join(self.start_dir,
                                 CONTENT_DIR, f_name)
        fh = open(full_path, "r")
        content = fh.read()
        fh.close()
        front_matter = content.split("+++")[1]
        fm_dict = yaml.safe_load(front_matter)
        fm = FrontMatter(**fm_dict)
        return fm

    def _get_content_type(self, f_name):
        return f_name.split('/')[0]

    def _get_body(self, f_name):
        pass

    def _get_file_list(self):
        content_path = os.path.join(self.start_dir, CONTENT_DIR)
        result = []
        for root, dirs, files in os.walk(content_path):
            rel_root = root.split(CONTENT_DIR)[1]
            result.extend([os.path.join(rel_root, f).strip('/')
                           for f in files])
        return result
