# -*- coding: utf-8 -*-


import datetime


class FrontMatter(object):

    def __init__(self, *args, **kwargs):
        self.categories = kwargs.get('categories', [])
        self.description = kwargs.get('description', "")
        self.draft = kwargs.get('draft', False)
        self.datetime = kwargs.get('datetime', datetime.datetime.now())


class Content(object):

    def __init__(self, front_matter, *args, **kwargs):
        self.front_matter = front_matter
        self.content_type = kwargs.get('content_type', '')
        self.file_path = kwargs.get('file_path', '')
