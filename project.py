# -*- coding: utf-8 -*-

import sys

from utils import check_if_project_present


class Project(object):

    def __init__(self, start_dir="./"):
        if not check_if_project_present(start_dir):
            sys.stderr.write(
                "No project found. Try to bootstrap new project first.")
            sys.exit(1)
        self.start_dir = start_dir
