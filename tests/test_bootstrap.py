# -*- coding: utf-8 -*-

import os
import shutil
import unittest
import yaml

from bootstrap import Bootstrap
from config import (
    BOOTSTRAP_CONFIG,
    CONFIG_FILE,
    DEFAULT_THEME_DIR,
    DIR_STRUCTURE,
    FILE_STRUCTURE,
    SAMPLE_THEME_DIR,
    THEMES_DIR,
)


class BootstrapTestCase(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"

    def tearDown(self):
        shutil.rmtree(self.start_directory)

    def test_directories_creation(self):
        bs = Bootstrap(self.start_directory)
        bs.start()
        for d in DIR_STRUCTURE:
            full_dir = os.path.join(self.start_directory, d)
            self.assertTrue(os.path.exists(full_dir))

    def test_files_creation(self):
        bs = Bootstrap(self.start_directory)
        bs.start()
        for f in FILE_STRUCTURE:
            full_path = os.path.join(self.start_directory, f)
            self.assertTrue(os.path.isfile(full_path))

    def test_config_file_contents(self):
        bs = Bootstrap(self.start_directory)
        bs.start()
        full_path = os.path.join(self.start_directory, CONFIG_FILE)
        fh = open(full_path, "r")
        config = yaml.safe_load(fh.read())
        fh.close()
        self.assertDictEqual(BOOTSTRAP_CONFIG, config)

    def test_override_config_structure(self):
        bs = Bootstrap(start_dir=self.start_directory,
                       config={'some': 'config'})
        bs.start()
        full_path = os.path.join(self.start_directory, CONFIG_FILE)
        fh = open(full_path, "r")
        config = yaml.safe_load(fh.read())
        fh.close()
        self.assertDictEqual({'some': 'config'}, config)

    def test_default_theme_exists_in_structure(self):
        bs = Bootstrap(start_dir=self.start_directory,
                       config={'some': 'config'})
        bs.start()
        for root, dirs, files in os.walk(DEFAULT_THEME_DIR):
            for file in files:
                d = root.split(SAMPLE_THEME_DIR)[-1].lstrip('/')
                path = os.path.join(self.start_directory,
                                    THEMES_DIR, SAMPLE_THEME_DIR, d, file)
                self.assertTrue(os.path.isfile(path))
