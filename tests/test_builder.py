# -*- coding: utf-8 -*-

import os
import shutil
import unittest

from bootstrap import Bootstrap
from builder import Builder
from config import (BOOTSTRAP_CONFIG, CONTENT_DIR, DEFAULT_THEME_DIR,
                    PUBLIC_DIR, STATIC_DIR)


class BuilderTestCase(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"
        bs = Bootstrap(self.start_directory)
        bs.start()

    def tearDown(self):
        shutil.rmtree(self.start_directory)

    def test_builder_gets_site_metadata_after_init(self):
        builder = Builder(self.start_directory)
        self.assertDictEqual(builder.site_metadata, BOOTSTRAP_CONFIG)

    def test_builder_build_static_files(self):
        builder = Builder(self.start_directory)
        builder.build()
        for root, dirs, files in os.walk(os.path.join(DEFAULT_THEME_DIR, STATIC_DIR)):
            for file in files:
                d = root.split(STATIC_DIR)[-1].lstrip('/')
                path = os.path.join(self.start_directory,
                                    PUBLIC_DIR, STATIC_DIR, d, file)
                self.assertTrue(os.path.isfile(path))

    def test_content_types(self):
        content_dir = os.path.join(self.start_directory, CONTENT_DIR)
        os.makedirs(os.path.join(content_dir, 'abc'))
        os.makedirs(os.path.join(content_dir, 'zxy'))
        builder = Builder(self.start_directory)
        builder.build()
        self.assertIn('abc', builder.site_contenttypes)
        self.assertIn('zxy', builder.site_contenttypes)
