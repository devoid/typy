import io
import os
import shutil
import unittest
from unittest import mock

from bootstrap import Bootstrap
from config import PUBLIC_DIR
from constructor import Constructor
from content import New
from data import Data, MetaData


class ConstructorTestCase(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"
        self.example_path = "post/empty-page.rst"
        bs = Bootstrap(self.start_directory)
        bs.start()

    def tearDown(self):
        shutil.rmtree(self.start_directory)

    def test_render_template(self):
        template = "<html>{{ a }} - {{ b }}</html>"
        context = {
            'a': 'a_value',
            'b': 'b_value',
        }
        expected_result = "<html>a_value - b_value</html>"
        constructor = Constructor(self.start_directory)
        result = constructor._render_from_text(template, context)
        self.assertEqual(result, expected_result)

    def test_render_template_without_variables(self):
        template = "<html>{{ a }} - {{ b }}</html>"
        context = {}
        expected_result = "<html> - </html>"
        constructor = Constructor(self.start_directory)
        result = constructor._render_from_text(template, context)
        self.assertEqual(result, expected_result)

    def test_render_template_from_file(self):
        fake_file = io.StringIO('data1')
        with mock.patch('constructor.open', return_value=fake_file, create=True):
            constructor = Constructor(self.start_directory)
            self.assertEqual(
                'data1', constructor._render_template('file', {}))

    def test_construct_html_structure(self):
        new_content = New(self.start_directory)
        new_content.create(self.example_path)
        data = Data(self.start_directory)
        content_objects = data.get_content_data()
        meta_data = MetaData(self.start_directory)
        site_metadata = meta_data.gather_config_metadata()
        constructor = Constructor(self.start_directory, metadata=site_metadata)
        constructor.construct(content_objects)
        self.assertTrue(os.path.isfile(os.path.join(
            self.start_directory, PUBLIC_DIR, "index.html")))
        self.assertTrue(os.path.isfile(os.path.join(
            self.start_directory, PUBLIC_DIR, 'post', 'index.html')))
        self.assertTrue(os.path.isfile(os.path.join(
            self.start_directory, PUBLIC_DIR, 'post', 'empty-page', 'index.html')))
