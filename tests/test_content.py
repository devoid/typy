# -*- coding: utf-8 -*-
import os
import shutil
import unittest
import yaml

from bootstrap import Bootstrap
from config import CONTENT_DIR, FRONT_MATTER
from content import New


class ContentTestCase(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"
        self.example_path = "post/empty-page.rst"
        bs = Bootstrap(self.start_directory)
        bs.start()

    def tearDown(self):
        shutil.rmtree(self.start_directory)

    def test_create_new_content(self):
        new_content = New(self.start_directory)
        new_content.create(self.example_path)
        self.assertTrue(os.path.isfile(os.path.join(
            self.start_directory, CONTENT_DIR, self.example_path)))

    def test_content_file_initial_content(self):
        new_content = New(self.start_directory)
        new_content.create(self.example_path)
        full_path = os.path.join(self.start_directory,
                                 CONTENT_DIR, self.example_path)
        fh = open(full_path, "r")
        content = fh.read()
        fh.close()
        front_matter = content.split("+++")[1]
        fm_dict = yaml.safe_load(front_matter)
        self.assertDictEqual(FRONT_MATTER, fm_dict)

    def test_override_content_file_initial_content(self):
        new_content = New(start_dir=self.start_directory,
                          front_matter={'something': 'else'})
        new_content.create(self.example_path)
        full_path = os.path.join(self.start_directory,
                                 CONTENT_DIR, self.example_path)
        fh = open(full_path, "r")
        content = fh.read()
        fh.close()
        front_matter = content.split("+++")[1]
        fm_dict = yaml.safe_load(front_matter)
        self.assertDictEqual({'something': 'else'}, fm_dict)
