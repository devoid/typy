# -*- coding: utf-8 -*-

import shutil
import unittest

from bootstrap import Bootstrap
from config import FRONT_MATTER
from content import New
from data import Data


class DataTest(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"
        self.example_path = "post/my-first-great-post.md"
        bs = Bootstrap(self.start_directory)
        bs.start()

    def tearDown(self):
        shutil.rmtree(self.start_directory)

    def test_get_content_type(self):
        data = Data(self.start_directory)
        file_path = "post/something.md"
        self.assertTrue(data._get_content_type(file_path), "post")

    def test_get_content_data(self):
        new_content_count = 1
        new_content = New(self.start_directory)
        new_content.create(self.example_path)
        data = Data(self.start_directory)
        self.assertEqual(len(data.get_content_data()), new_content_count)

    def test_get_front_matter(self):
        new_content = New(self.start_directory)
        new_content.create(self.example_path)
        data = Data(self.start_directory)
        fm = data._get_front_matter(self.example_path)
        self.assertEqual(fm.categories, FRONT_MATTER['categories'])
        self.assertEqual(fm.description, FRONT_MATTER['description'])
        self.assertEqual(fm.draft, False)
        self.assertEqual(fm.datetime, FRONT_MATTER['datetime'])
