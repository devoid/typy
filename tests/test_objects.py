# -*- coding: utf-8 -*-

import datetime
import unittest

from objects import Content, FrontMatter


class FrontMatterTest(unittest.TestCase):

    def test_front_matter_all_set_attributes(self):
        timestamp = datetime.datetime.now()
        data = {
            'categories': ['abc', 'cdf', ],
            'description': "lorem ipsum",
            'draft': True,
            'datetime': timestamp,
        }
        fm = FrontMatter(**data)
        self.assertEqual(data['categories'], fm.categories)
        self.assertEqual(data['description'], fm.description)
        self.assertEqual(data['draft'], fm.draft)
        self.assertEqual(data['datetime'], fm.datetime)

    def test_front_matter_without_attributes(self):
        fm = FrontMatter()
        self.assertEqual(fm.categories, [])
        self.assertEqual(fm.description, "")
        self.assertEqual(fm.draft, False)
        self.assertTrue(fm.datetime)


class ContentTestCase(unittest.TestCase):

    def test_content_attributes(self):
        timestamp = datetime.datetime.now()
        data = {
            'categories': ['abc', 'cdf', ],
            'description': "lorem ipsum",
            'draft': True,
            'datetime': timestamp,
        }
        fm = FrontMatter(**data)
        content = Content(fm, content_type="abcd")
        self.assertEqual(content.front_matter, fm)
        self.assertEqual(content.content_type, 'abcd')
