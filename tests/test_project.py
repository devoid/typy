# -*- coding: utf-8 -*-


import shutil
import unittest

from bootstrap import Bootstrap
from project import Project


class ProjectTestCase(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"
        self.not_existing_start_directory = "./not_test_site"

    def tearDown(self):
        shutil.rmtree(self.start_directory)

    def test_project_success_with_project_structure(self):
        bs = Bootstrap(self.start_directory)
        bs.start()
        Project(self.start_directory)

    def test_project_fail_without_project_structure(self):
        bs = Bootstrap(self.start_directory)
        bs.start()
        self.assertRaises(SystemExit, Project,
                          self.not_existing_start_directory)
