# -*- coding: utf-8 -*-

import shutil
import unittest

from bootstrap import Bootstrap
from utils import check_if_project_present, render_template


class UtilsTestCase(unittest.TestCase):

    def setUp(self):
        self.start_directory = "./test_site"

    def tearDown(self):
        try:
            shutil.rmtree(self.start_directory)
        except OSError:
            pass

    def test_check_if_project_present_when_project_exists(self):
        bs = Bootstrap(self.start_directory)
        bs.start()
        self.assertTrue(check_if_project_present(self.start_directory))

    def test_check_if_project_present_when_project_does_not_exists(self):
        self.assertFalse(check_if_project_present(self.start_directory))

    def test_render_template(self):
        template = "<html>{{ a }} - {{ b }}</html>"
        context = {
            'a': 'a_value',
            'b': 'b_value',
        }
        expected_result = "<html>a_value - b_value</html>"
        result = render_template(template, context)
        self.assertEqual(result, expected_result)

    def test_render_template_without_variables(self):
        template = "<html>{{ a }} - {{ b }}</html>"
        context = {}
        expected_result = "<html> - </html>"
        result = render_template(template, context)
        self.assertEqual(result, expected_result)
