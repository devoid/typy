#!/usr/bin/python
#
# -*- coding: utf-8 -*-

import argparse
import sys
from bootstrap import Bootstrap
from builder import Builder
from content import New
from utils import check_if_project_present


def bootstrap(args):
    bs = Bootstrap(args.start_directory)
    bs.start()


def new(args):
    if check_if_project_present(args.start_directory):
        new = New(args.start_directory)
        new.create(args.path)
    else:
        sys.stderr.write(
            "No project found. Try to bootstrap new project first.")


def build(args):
    if check_if_project_present(args.start_directory):
        builder = Builder(args.start_directory)
        builder.build()
    else:
        sys.stderr.write(
            "No project found. Try to bootstrap new project first.")


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command_name")

parser_bootstrap = subparsers.add_parser(
    "bootstrap", help="bootstrap typy project")
parser_bootstrap.add_argument(
    "-d", "--start-directory", type=str, help="directory to start project in", default="./")
parser_bootstrap.set_defaults(func=bootstrap)

parser_new = subparsers.add_parser(
    "new", help="new content for project")
parser_new.add_argument(
    "path", type=str, action="store", help="path to new content")
parser_new.add_argument(
    "-d", "--start-directory", type=str, help="typy project directory", default="./")
parser_new.set_defaults(func=new)

parser_build = subparsers.add_parser(
    "build", help="build typy project")
parser_build.add_argument(
    "-d", "--start-directory", type=str, help="typy project directory", default="./")
parser_build.set_defaults(func=build)

args = parser.parse_args()
args.func(args)
