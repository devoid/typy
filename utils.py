# -*- coding: utf-8 -*-

import os
from jinja2 import Template
from config import DIR_STRUCTURE


def check_if_project_present(directory):
    for d in DIR_STRUCTURE:
        full_dir = os.path.join(directory, d)
        if not os.path.exists(full_dir):
            return False
    return True


def render_template(template, context):
    template = Template(template)
    return template.render(context)
